#!/usr/bin/env python3

import gi
import sys
import threading
import time

gi.require_version("Gst", "1.0")
from gi.repository import GLib, Gst  # nopep8


layout1 = {"width": 320, "height": 240, "xpos": 0, "ypos": 0}
layout2 = {"width": 320, "height": 240, "xpos": 0, "ypos": 240}
layout3 = {"width": 320, "height": 240, "xpos": 0, "ypos": 240}
layout4 = {"width": 320, "height": 240, "xpos": 0, "ypos": 0}
active_layout = {0: [layout1, layout2], 1: [layout3, layout4]}


class Layouts:
    def __init__(self):
        self.loop = None
        self.pipeline = None
        self.videosrc1 = None
        self.videosrc2 = None
        self.videoconvert1 = None
        self.videoconvert2 = None
        self.compositor = None
        self.audiosrc1 = None
        self.audiosrc2 = None
        self.audiomixer = None
        self.audiosink = None
        self.currentlayout = 0
        self.compositor_pad1 = None
        self.compositor_pad2 = None

    def init_pipeline(self):
        self.videosrc1 = Gst.ElementFactory.make("videotestsrc")
        self.videosrc2 = Gst.ElementFactory.make("videotestsrc")
        self.videoconvert = Gst.ElementFactory.make("videoconvert")
        self.videosink = Gst.ElementFactory.make("autovideosink")

        self.audiosrc1 = Gst.ElementFactory.make("audiotestsrc")
        self.audiosrc2 = Gst.ElementFactory.make("audiotestsrc")
        self.audiomixer = Gst.ElementFactory.make("audiomixer")
        self.audiosink = Gst.ElementFactory.make("pulsesink")

        self.compositor = Gst.ElementFactory.make("compositor")

        self.videosrc1.set_property("pattern", "checkers-8")
        self.videosrc2.set_property("pattern", "blue")

        self.audiosrc1.set_property("wave", "sine")
        self.audiosrc2.set_property("wave", "ticks")
        self.audiosrc2.set_property("tick-interval", 1000000000)
        self.audiosink.set_property("volume", 1)

    def create_pipeline(self):
        self.pipeline = Gst.Pipeline.new()
        self.pipeline.add(
            self.videosrc1,
            self.videosrc2,
            self.videoconvert,
            self.videosink,
            self.audiosrc1,
            self.audiosrc2,
            self.audiomixer,
            self.audiosink,
            self.compositor,
        )
        self.pipeline.sync_children_states()

        audiomix1_pad = self.audiomixer.get_request_pad("sink_%u")
        audiosrc1_pad = self.audiosrc1.get_static_pad("src")
        audiosrc1_pad.link(audiomix1_pad)

        audiomix2_pad = self.audiomixer.get_request_pad("sink_%u")
        audiosrc2_pad = self.audiosrc2.get_static_pad("src")
        audiosrc2_pad.link(audiomix2_pad)

        self.audiomixer.link(self.audiosink)

        compositor1_pad = self.compositor.get_request_pad("sink_%u")
        compositor1_pad.set_property("width", layout1["width"])
        compositor1_pad.set_property("height", layout1["height"])
        compositor1_pad.set_property("xpos", layout1["xpos"])
        compositor1_pad.set_property("ypos", layout1["ypos"])
        videosrc1_pad = self.videosrc1.get_static_pad("src")
        videosrc1_pad.link(compositor1_pad)

        compositor2_pad = self.compositor.get_request_pad("sink_%u")
        compositor2_pad.set_property("width", layout2["width"])
        compositor2_pad.set_property("height", layout2["height"])
        compositor2_pad.set_property("xpos", layout2["xpos"])
        compositor2_pad.set_property("ypos", layout2["ypos"])
        videosrc2_pad = self.videosrc2.get_static_pad("src")
        videosrc2_pad.link(compositor2_pad)

        self.compositor.link(self.videosink)

        self.compositor_pad1 = compositor1_pad
        self.compositor_pad2 = compositor2_pad

    def compositor1_buffer_idle_probe_callback(self, pad, info):
        layout = active_layout[self.currentlayout][0]

        pad.set_property("width", layout["width"])
        pad.set_property("height", layout["height"])
        pad.set_property("xpos", layout["xpos"])
        pad.set_property("ypos", layout["ypos"])

        return Gst.PadProbeReturn.REMOVE

    def compositor2_buffer_idle_probe_callback(self, pad, info):
        layout = active_layout[self.currentlayout][1]

        pad.set_property("width", layout["width"])
        pad.set_property("height", layout["height"])
        pad.set_property("xpos", layout["xpos"])
        pad.set_property("ypos", layout["ypos"])

        return Gst.PadProbeReturn.REMOVE

    def compositor1_buffer_probe_callback(self, pad, info):
        pad.add_probe(
            Gst.PadProbeType.BUFFER,
            self.compositor1_buffer_idle_probe_callback
        )
        return Gst.PadProbeReturn.REMOVE

    def compositor2_buffer_probe_callback(self, pad, info):
        pad.add_probe(
            Gst.PadProbeType.BUFFER,
            self.compositor2_buffer_idle_probe_callback
        )
        return Gst.PadProbeReturn.REMOVE

    def start_pipeline(self):
        self.loop = GLib.MainLoop()
        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect("message", self.bus_msg_handler)
        # bus.connect("message::element", self.bus_level_msg_handler)
        self.pipeline.set_state(Gst.State.PLAYING)
        # https://stackoverflow.com/questions/8600161/executing-periodic-actions-in-python
        timerThread = threading.Thread(target=self.periodic_callback)
        timerThread.daemon = True
        timerThread.start()
        try:
            self.loop.run()
        except Exception:
            pass

    def close_pipeline(self):
        self.pipeline.get_bus().remove_signal_watch()
        self.pipeline.set_state(Gst.State.NULL)
        self.pipeline = None

    def bus_msg_handler(self, bus, message):
        t = message.type
        if t == Gst.MessageType.EOS:
            sys.stdout.write("End-of-stream\n")
            self.loop.quit()
        elif t == Gst.MessageType.ERROR:
            err, debug = message.parse_error()
            sys.stderr.write("Error: %s: %s\n" % (err, debug))
            self.loop.quit()
        return True

    def periodic_callback(self):
        next_call = time.time()
        while True:
            if self.currentlayout == 0:
                self.currentlayout = 1
            else:
                self.currentlayout = 0

            self.compositor_pad1.add_probe(
                Gst.PadProbeType.BUFFER,
                self.compositor1_buffer_probe_callback
            )
            self.compositor_pad2.add_probe(
                Gst.PadProbeType.BUFFER,
                self.compositor2_buffer_probe_callback
            )
            next_call = next_call + 1
            time.sleep(next_call - time.time())

    # def bus_level_msg_handler(self, bus, message):
    #     s = message.get_structure()
    #     name = s.get_name()
    #     if name == 'level':
    #         # We can get the number of channels as the length of any of the
    #         # value lists
    #         # channels = len(s['rms'])
    #         list = s['rms']
    #         if self.last_level == -1:
    #             self.last_level = abs(list[0])
    #         else:
    #             level = abs(list[0])
    #             diff = abs(self.last_level - level)
    #             if diff > 0.5:
    #                 self.compositor_pad1.add_probe(
    #                     Gst.PadProbeType.BUFFER,
    #                     self.compositor1_buffer_probe_callback
    #                 )
    #                 self.compositor_pad2.add_probe(
    #                     Gst.PadProbeType.BUFFER,
    #                     self.compositor2_buffer_probe_callback
    #                 )
    #             self.last_level = level
    #     return True


if __name__ == "__main__":
    Gst.init(None)
    layout = Layouts()
    layout.init_pipeline()
    layout.create_pipeline()
    layout.start_pipeline()
    layout.close_pipeline()
